﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class RankingJuego : MonoBehaviour {

	public int tiempoRecord = 0;

	public static RankingJuego rankingJuego;
	
	public String rutaArchivo;

	void Awake(){
		rutaArchivo = Application.persistentDataPath + "/datos.dat"; //ruta archivo
		//si no existe lo crea y si ya existe se destruye
		if(rankingJuego == null)
		{
			rankingJuego = this;
			DontDestroyOnLoad(gameObject);
		}
		else if(rankingJuego != this)
		{
			Destroy(gameObject);
		}
	}

	// Use this for initialization
	void Start () 
	{
		Cargar(); 
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	public void Guardar() //guarda los datos
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(rutaArchivo);
		
		DatosAGuardar datos = new DatosAGuardar();
		datos.tiempoRecord  = tiempoRecord;
		
		bf.Serialize(file, datos);
		
		file.Close();
	}
	
	void Cargar(){
		if(File.Exists(rutaArchivo))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(rutaArchivo, FileMode.Open);
			
			DatosAGuardar datos = (DatosAGuardar) bf.Deserialize(file);
			
			tiempoRecord = datos.tiempoRecord;
			
			file.Close();
		}else
		{
			tiempoRecord = 0;
		}
	}
}

[Serializable]
class DatosAGuardar{
	public int tiempoRecord;
}