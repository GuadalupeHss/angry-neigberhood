﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HealthBar : MonoBehaviour
{
    /// <summary>
    //ESTA SIN HACER NI VINCULAR
    /// </summary>
    public Scrollbar HealthScrollbar;
    public float currentHealth = 100;


    public void Damage(float value)
    {
        currentHealth -= value;
        
        HealthScrollbar.size = currentHealth / 100f;
        
    }


}

