﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Crono : MonoBehaviour 
{

public Text timerText;
public float starttime;
public bool finnished = false;

	// Use this for initialization
	void Start () 
	{
		starttime = Time.time; //inicio
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(finnished)return; 
		float t = Time.time - starttime; // t es el tiempo transcurrido menos el tiempo de inicio

		string minutes = ((int) t / 60).ToString(); //t a minutos dividiendo entre 60 y pasadolo a texto
		string seconds = ((int) t % 60).ToString(); //t segundo dividiendo elresto entre 60 y redondeadolo a 2 decimales

		timerText.text = "0 : " + minutes + " : " + seconds; //actualiza el texto que muestra el tiempo
	}

	public void Finnish () //cuando se detiene el tiempo elboleanoes cierto y el texto se congela en rojo
	{
	finnished = true;
	timerText.color = Color.red;
	}

}
