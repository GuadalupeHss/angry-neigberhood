﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tiempo : MonoBehaviour {

	public int tiempoRecord = 0;
	public TextMesh marcador;

	// Use this for initialization
	void Start () 
	{
		NotificationCenter.DefaultCenter().AddObserver(this, "IncrementarTiempo");
		NotificationCenter.DefaultCenter().AddObserver(this, "PersonajeHaMuerto");
		ActualizarMarcador();
	}
	
	void PersonajeHaMuerto(Notification notificacion)
	{
		if(tiempoRecord < RankingJuego.rankingJuego.tiempoRecord){
			RankingJuego.rankingJuego.tiempoRecord = tiempoRecord;
			RankingJuego.rankingJuego.Guardar();
		}
	}

	void IncrementarTiempo (Notification notificacion)
	{
		int tiempoAIncrementar = (int)notificacion.data;
		tiempoRecord += tiempoAIncrementar;
		ActualizarMarcador();
	}

	void ActualizarMarcador()
	{
		marcador.text = tiempoRecord.ToString();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
