﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour {
    //Referencia estatica a la clase
    private static Singleton _instance;
    //Podemos coger la referencia pero no modificarla
    public static Singleton Instance{ get; private set;}
    //La vida que tiene el personaje
    public int currentHealth = 100;
    public static int newCurrentHealth = 0;
    public float currentTime = 0;
    public static float newCurrentTime = 0;
    public int currentSpeed =1;
    public static int newCurrentSpeed =1;
    // Use this for initialization
    void Awake() 
	{
        //Comprobamos si existe una instancia, en caso que exista, vemos si es este objeto, sino destruimos el objeto
        if (Instance != null && Instance != this) 
		{
            Destroy (gameObject);
        } else 
		{
            //No existe instancia Guardamos nuestra referencia
            Instance = this;
            //Le decimos a unity que no destruya el objeto entre escenas
            DontDestroyOnLoad(gameObject);
         }
    }
}
    /* 
    //Pedimos la vida que le queda al personaje
    public int getPlayercurrentHealth()
    {
        return currentHealth;
        Debug.Log("i've got current health");
    }
    //Modificamos la vida que le queda al personaje
    public void setPlayercurrentHealth(int newcurrentHealth)
    {
        currentHealth = newcurrentHealth;
        Debug.Log("i've set current heath");
    }

    public float getPlayercurrentTime()
    {
        return currentTime;
    }
     public void setPlayercurrentTime(int newcurrentTime)
       { 
           currentTime = newcurrentTime;
        }
        public int getPlayercurrentSpeed()
    {
        return currentSpeed;
        Debug.Log("i've got current speed");
    }
    //Modificamos la vida que le queda al personaje
    public void setPlayercurrentSpeed(int newcurrentSpeed)
    {
        currentSpeed = newcurrentSpeed;
        Debug.Log("i've set current speed");
    }
    */
