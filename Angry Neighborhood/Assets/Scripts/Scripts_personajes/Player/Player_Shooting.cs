﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player_Shooting : MonoBehaviour {

	// Use this for initialization	public float maxSpeed = 5f;
	
    public GameObject mProyectileFire;
	public float mReloadFire;
	public float mProjectileSpeed;

	
	


	private float mLastShotTime = 0.0f;
	
	
	


	// Use this for initialization
	void Start () {
		
		mLastShotTime = -mReloadFire;
		
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void FixedUpdate(){

		if (Input.GetKeyDown (KeyCode.Mouse1) && Time.time > mLastShotTime + mReloadFire && this.GetComponentInParent<Player_Controller>().grounded==true) {
			mLastShotTime = Time.time;
			this.GetComponentInParent<Animator>().SetTrigger("Shot");

			}


			
			
			//GameObject currentProjectile = Instantiate (mProyectileFire);

   //         if(this.GetComponentInParent<Player_Controller>().derecha == true){	
			//currentProjectile.transform.position = transform.position + 2.0f * transform.right;
			//currentProjectile.GetComponent<Rigidbody2D> ().AddForce (transform.right * mProjectileSpeed );
			//}

			//if(this.GetComponentInParent<Player_Controller>().derecha == false){
			//currentProjectile.transform.localScale = new Vector2 (-17.6953f,11.35954f);
   //         currentProjectile.transform.position = transform.position + 2.0f * -transform.right;
			//currentProjectile.GetComponent<Rigidbody2D> ().AddForce (-transform.right * mProjectileSpeed );
			//}
		}
	

    public void Shot()
    {
        GameObject currentProjectile = Instantiate(mProyectileFire);
       
		

        if (this.GetComponentInParent<Player_Controller>().derecha == true)
        {
            currentProjectile.transform.position = transform.position + 2.0f * transform.right;
            currentProjectile.GetComponent<Rigidbody2D>().AddForce(transform.right * mProjectileSpeed);
        }

        if (this.GetComponentInParent<Player_Controller>().derecha == false)
        {
            currentProjectile.transform.localScale = new Vector2(-17.6953f, 11.35954f);
            currentProjectile.transform.position = transform.position + 2.0f * -transform.right;
            currentProjectile.GetComponent<Rigidbody2D>().AddForce(-transform.right * mProjectileSpeed);
        }

    }




	}

