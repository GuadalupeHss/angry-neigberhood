﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour {

	public float maxSpeed = 5f;
	public float speed = 2f;
	public bool grounded;
	public float jumpPower = 6.5f;
	public bool derecha = true; 
   	public bool atacando;

	private Rigidbody2D rb2d;
	private Animator anim;
	private bool jump;
	private Player_Shot_Uzi player;
	private bool doublejump;

	public float contador = 0f;
	public float contadorMax=1f;


	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		player = GetComponent<Player_Shot_Uzi>();
		
	}
	
	// Update is called once per frame
	void Update () {
		anim.SetFloat ("Speed", Mathf.Abs (rb2d.velocity.x));
		anim.SetBool ("Grounded", grounded);

		if (Input.GetKeyDown (KeyCode.W) ) {
			if(grounded){
              jump = true;
			  doublejump = true;
			}else if(doublejump){
				jump=true;
				doublejump=false;

			}
			
		}

	}
	void FixedUpdate(){
       /* Vector3 fixedVelocity = rb2d.velocity;
		fixedVelocity.x*=0.75f;
		if (grounded){
			rb2d.velocity=fixedVelocity;
		}
		*/
		float h = Input.GetAxis("Horizontal");

		rb2d.AddForce(Vector2.right * speed * h);

		float limitedSpeed = Mathf.Clamp(rb2d.velocity.x, -maxSpeed, maxSpeed);
		rb2d.velocity = new Vector2(limitedSpeed, rb2d.velocity.y);

		if (h > 0.1f) {
			transform.localScale = new Vector3(1f, 1f, 1f);
			derecha = true;
		} 

		if (h < -0.1f){
			transform.localScale = new Vector3(-1f, 1f, 1f);
			derecha = false;
		}

		if (jump){
			rb2d.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
			jump = false;
		}
		if (Input.GetKeyDown (KeyCode.Mouse0) /*&& player.disparo == false*/) {
			anim.SetTrigger ("Attack_Knife");
			StopAllCoroutines();
			StartCoroutine ("tiempoAtacando");
		}
		if ( Input.GetKey(KeyCode.S)){
		     anim.SetBool("Agachado",true);
		}else
		     anim.SetBool("Agachado",false);
		
	}

	IEnumerator tiempoAtacando(){
		atacando = true;
		yield return new WaitForSeconds (0.5f);
		atacando = false;
	}

	void OnTriggerStay2D (Collider2D other)
	{ 
		if (other.gameObject.CompareTag ("Enemy") && atacando) 
		{
			contador = contador - Time.deltaTime;
		}
			if(contador<=0f)
			{
			Debug.Log ("quitavida");
			other.gameObject.GetComponentInChildren<Yonki> ().salud--;
				contador =	contadorMax;
		    }
		}
		
}
