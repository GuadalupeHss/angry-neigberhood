﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Health : MonoBehaviour {

	public int startingHealth = 100;
    public  int currentHealth;
    public Player_Controller player_Controller;
    public AudioClip muerte_Sound;
   
    
    private SpriteRenderer spr;


    Animator anim;
    AudioSource muerte;
    

    bool isDead;
    bool damaged;


    void Awake()
    {
       anim = GetComponent<Animator>();
       muerte = GetComponent<AudioSource>(); 
       currentHealth = startingHealth;
    }

 
    void Update()
    {
     
      
    }

    
    public void TakeDamage(int amount)
    {
        damaged = true;
        currentHealth -= amount;
        
        if (currentHealth <= 0 && !isDead)
        {
            anim.SetTrigger("Die");
            muerte.clip=muerte_Sound;
            muerte.Play();
            player_Controller.enabled = false;
        }
    }

    
    
    

}
