﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chech_Ground : MonoBehaviour {

	private Player_Controller player;

	// Use this for initialization
	void Start () {
		player = GetComponentInParent<Player_Controller>();
	}
	
	void OnCollisionStay2D(Collision2D col){
		if(col.gameObject.tag == "Ground"){
			player.grounded = true;
		}
	}

	void OnCollisionExit2D(Collision2D col){
		if(col.gameObject.tag == "Ground"){
			player.grounded = false;
		}
	}
}
