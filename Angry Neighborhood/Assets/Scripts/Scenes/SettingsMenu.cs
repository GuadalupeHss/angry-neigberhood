﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class SettingsMenu : MonoBehaviour 
{
	public AudioMixer audioMixer;
	public AudioMixer musicMixer;
	public void Setvolumen( float volumen)
	{
		audioMixer.SetFloat("volumen", volumen);
	}

		public void SetMusicVolumen( float volumen)
	{
		musicMixer.SetFloat("musicvolumen", volumen);
	}

	public void SetQuality(int qualityIndex)
	{
		QualitySettings.SetQualityLevel(qualityIndex);
	}

	public void SetFullScreen (bool isFullScreen)
	{
		Screen.fullScreen = isFullScreen;
	}
}
