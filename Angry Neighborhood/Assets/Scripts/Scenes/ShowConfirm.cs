﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class ShowConfirm : MonoBehaviour 
{

    public GameObject panel;  
   
    public void TogglePanel (GameObject panel) //muestra el diálogo
	{
        panel.SetActive (!panel.activeSelf);
    }


    public void ClosePanel (GameObject panel) // esconde el diálogo
	{
		panel.SetActive (false);
    }
}
