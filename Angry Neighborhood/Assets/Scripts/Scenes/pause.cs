using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pause : MonoBehaviour {
    public bool active;
    public GameObject panel;  
   


	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) //si se presiona ESC saca el panel pausa
        {
            ShowPanel(panel);
            Time.timeScale = (active) ? 0 : 1f;
        }
	}

        public void ShowPanel (GameObject panel) //muestra el panel
	{
        panel.SetActive (!panel.activeSelf);
    }

        public void ClosePanel (GameObject panel) // esconde el panel
	{
		panel.SetActive (false);
    }
}
